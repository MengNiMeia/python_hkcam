# python调用海康网络SDK访问IPC
  基于海康demo封装,利用ctypes访问海康SDK

使用方法(单路):
```
form HKCam import HKCam
camIP ='192.168.1.122'
DEV_PORT = 8000
username ='admin' #用户名
password = 'admin' #密码
hkclass = HKCam(camIP,username,password)
last_stamp = 0
while True:
    n_stamp,img = hkclass.read()
    last_stamp=n_stamp
    cv2.imshow('xxxxxx',cv2.resize(img,(800,600)))
    kkk = cv2.waitKey(1)
    if kkk ==ord('q'):
        break
hkclass.release()
```

使用方法(多路)：

    form HKCam_multi import HKCam_mul
    camIPS =['192.168.3.151',
                '192.168.3.157',   
                '192.168.3.167',
                '192.168.3.119',                       
                '192.168.3.162',
                '192.168.3.103']
    usernames =['admin']*6
    passwords = ['admin']*6
    hkcam_muls = HKCam_mul(camIPS,usernames,passwords)
    while True:
        t0 = time.time()
        imgs = hkcam_muls.read()
        for key in imgs.keys():
            stamp,img = imgs[key]
            cv2.imshow(f'{key}',cv2.resize(img,(800,600)))
            kkk =cv2.waitKey(1)
        if kkk==ord('q'):
            break
    hkcam_muls.release()



更多内容请参考海康官方SDK:

https://open.hikvision.com/download/5cda567cf47ae80dd41a54b3?type=10

